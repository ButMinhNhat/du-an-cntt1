module.exports = {
  content: ["./src/**/*.{html,js}"],
  important: "#root",
  theme: {
    extend: {},
  },
  plugins: [
	require('@tailwindcss/line-clamp'),
  ],
};
