import { gql } from "@apollo/client/core";
import GraphQLClient from "../config";

const GET_TOPICS = gql`
  query GetTopic {
    getTopics {
      _id
      name
	  slug
      parent {
        _id
        name
      }
    }
  }
`;

export const getTopics = async () => {
  try {
    let result = await GraphQLClient.query({
      query: GET_TOPICS,
    });

    return result.data?.getTopics ?? [];
  } catch (error) {
    console.log(error);
  }
};
