import { gql } from "@apollo/client/core";
import GraphQLClient from "../config";

const QUERY_TRACKING = gql`
  query QueryTracking($type: String!, $courseId: String!) {
    queryTracking(type: $type, courseId: $courseId) {
      _id
      type
      data
    }
  }
`;

export const queryTracking = async (type, courseId) => {
  try {
    let result = await GraphQLClient.query({
      query: QUERY_TRACKING,
      fetchPolicy: "no-cache",
      variables: { type, courseId },
    });

    return {
      data: result.data?.queryTracking,
    };
  } catch (error) {
    console.log(error);
    return {
      error: error,
    };
  }
};

const QUERY_TRACKING_FOR_COURSES = gql`
  query QueryTrackingForCourses($type: String!, $courseIds: [String!]) {
    queryTrackingForCourses(type: $type, courseIds: $courseIds) {
      _id
      type
      data
	  course {
		_id
	  }
    }
  }
`;

export const queryTrackingForCourses = async (type, courseIds) => {
  try {
    let result = await GraphQLClient.query({
      query: QUERY_TRACKING_FOR_COURSES,
      fetchPolicy: "no-cache",
      variables: { type, courseIds },
    });

    return {
      data: result.data?.queryTrackingForCourses,
    };
  } catch (error) {
    console.log(error);
    return {
      error: error,
    };
  }
};
