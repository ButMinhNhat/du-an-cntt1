import { gql } from "@apollo/client/core";
import GraphQLClient from "../config";

const GET_TRANSACTIONS_BY_TYPES = gql`
  query QueryTransactionByTypes($types: [String!], $courseId: String!) {
    queryTransactionsByType(types: $types, courseId: $courseId) {
      _id
      type
      course {
        _id
      }
      parent
      content {
        value
        text
        answers {
          text
          chooses
        }
        score
      }
	  createdBy {
		_id
		name
		avatar
	  }
	  createdAt
    }
  }
`;

export const getTransactionByTypes = async (types = [], courseId) => {
  try {
    let result = await GraphQLClient.query({
      query: GET_TRANSACTIONS_BY_TYPES,
      fetchPolicy: "no-cache",
	  variables: { types, courseId }
    });

    return {
		data: result.data?.queryTransactionsByType,
	}
  } catch (error) {
    console.log(error);
	return {
		error: error,
	};
  }
};
