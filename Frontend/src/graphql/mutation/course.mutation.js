import { gql } from "@apollo/client/core";
import GraphQLClient from "../config";

const COURSE_MUTATION = gql`
  mutation mutationCourse($input: CourseInput!) {
    mutationCourse(input: $input) {
      title
      status
      sections {
        title
        lectures {
          title
          media
          files
          content
          sequence
        }
        quiz {
          _id
        }
      }
      shortDescription
      description
      coverImage
      level
      hashtags
	  features
      topic {
        _id
        name
      }
      createdBy {
        _id
        name
      }
    }
  }
`;

export const saveCourse = async (input) => {
  try {
    let result = await GraphQLClient.mutate({
      mutation: COURSE_MUTATION,
      variables: { input },
    });

    return {
      res: result.data?.mutationCourse,
    };
  } catch (error) {
    console.log(error);
    return {
      error: error,
    };
  }
};
