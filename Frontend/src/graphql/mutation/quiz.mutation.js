import { gql } from "@apollo/client/core";
import GraphQLClient from "../config";

const QUIZ_MUTATION = gql`
  mutation mutationQuiz($input: QuizInput!) {
    mutationQuiz(input: $input) {
      _id
      title
      questions {
        type
        title
        answers
        correctAnswers
      }
      sequence
    }
  }
`;

export const createQuiz = async (input) => {
  try {
    let result = await GraphQLClient.mutate({
      mutation: QUIZ_MUTATION,
      variables: { input },
    });

    return {
		data: result.data?.mutationQuiz
	};
  } catch (error) {
    console.log(error);
	return {
		error: error
	}
  }
};
