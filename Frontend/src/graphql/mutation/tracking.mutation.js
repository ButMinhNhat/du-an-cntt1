import { gql } from "@apollo/client/core";
import GraphQLClient from "../config";

const TRACKING_MUTATION = gql`
  mutation mutationTracking($input: TrackingInput!) {
    mutationTracking(input: $input) {
      _id
      type
      data
      course {
        _id
      }
    }
  }
`;

export const saveTracking = async (input) => {
  try {
    let result = await GraphQLClient.mutate({
      mutation: TRACKING_MUTATION,
      variables: { input },
    });

    return {
      data: result.data?.mutationTracking,
    };
  } catch (error) {
    console.log(error);
    return {
      error: error,
    };
  }
};
