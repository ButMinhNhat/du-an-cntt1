import { createSlice } from "@reduxjs/toolkit";
import * as Actions from "actions";

const authSlice = createSlice({
    name: 'auth',
    initialState:{
		login: (localStorage.getItem('user-info') && localStorage.getItem('token')) ? true : false,
		data: JSON.parse(localStorage.getItem('user-info')) ?? null,
		token: localStorage.getItem('token') ?? null,
		alert: {
			state: false,
			type: "success",
			content: ""
		}
	},
    reducers:{},
	extraReducers: (builder) => {
		builder.addCase(Actions.loginAccount.fulfilled, (state, action) => {
		    localStorage.setItem('user-info', JSON.stringify(action.payload.data))
			localStorage.setItem('token', action.payload.token)

			state.login = true
			state.data = action.payload.data
			state.token = action.payload.token
		});
		builder.addCase(Actions.setAlertSnackbar.fulfilled, (state, action) => {
			state.alert = action.payload
		})
	},	
});

const { reducer } = authSlice;
export default reducer;