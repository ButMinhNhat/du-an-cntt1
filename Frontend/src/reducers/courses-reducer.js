import { createSlice } from "@reduxjs/toolkit";
import * as Actions from "actions";

const coursesSlice = createSlice({
    name: 'courses',
    initialState:{
		data: null,
		tracking: null,
	},
    reducers:{},
	extraReducers: (builder) => {
		builder.addCase(Actions.getCoursesList.fulfilled, (state, action) => {
		    state.data = action.payload
		});
		builder.addCase(Actions.getCoursesInstructor.fulfilled, (state, action) => {
		    state.data = action.payload
		});
		builder.addCase(Actions.getMyLearning.fulfilled, (state, action) => {
			state.data = action.payload
		});
		builder.addCase(Actions.clearCoursesListData, (state) => {
			state.data = null;
			state.tracking = null;
		});
		builder.addCase(Actions.getMyLearningTracking.fulfilled, (state, action) => {
			state.tracking = action.payload
		});
	},	
});

const { reducer } = coursesSlice;
export default reducer;