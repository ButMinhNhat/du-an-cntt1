import coursesSlice from './courses-reducer';
import courseSlice from './course-reducer';
import topicSlice from './topic-reducer';
import authSlice from './auth-reducer';

export const reducerSlice = {
	auth   : authSlice,
	courses: coursesSlice,
	course : courseSlice,
	topic  : topicSlice
}