import { createAsyncThunk } from "@reduxjs/toolkit";
import * as Actions from 'actions';

export const loginAccount = createAsyncThunk(
	'POST Login',
	async (data, { dispatch }) => {
		dispatch(Actions.setAlertSnackbar({
			state: true,
			type: "success",
			content: "Sign in success!"
		}));
		return data;
	}
)

export const setAlertSnackbar = createAsyncThunk(
	'Set Alert Snackbar',
	async (data) => {
		return data;
	}
)