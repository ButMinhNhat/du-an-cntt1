import { createAction, createAsyncThunk } from "@reduxjs/toolkit";
import * as queries from 'graphql/query';
import * as mutations from 'graphql/mutation';
import { trackingEvent } from "ultis/Common";
import * as Actions from 'actions';

export const createNewCourse = createAsyncThunk(
	'GET New course',
	async (data) => {
		return data;
	}
)

export const getCourseBySlug = createAsyncThunk(
	'GET Course by slug',
	async (slug, { dispatch }) => {
		const { data, error } = await queries.getCourseBySlug(slug);
		if(error) {
			dispatch(Actions.setAlertSnackbar({
				state: true,
				type: "error",
				content: "Get course fail"
			}));
		}
		if(data) return data;
	}
)

export const getCoursesInstructorBySlug = createAsyncThunk(
	'GET Course Intructor by slug',
	async (slug, { dispatch }) => {
		const { data, error } = await queries.getInstructorCourseBySlug(slug);
		if(error) {
			dispatch(Actions.setAlertSnackbar({
				state: true,
				type: "error",
				content: "Get course fail"
			}));
		}
		if(data) return data;
	}
)

export const clearCourseData = createAction('Clear course data');

export const getCourseTransactions = createAsyncThunk(
	'GET Course Transactions',
	async ({types = [], courseId}, { dispatch }) => {
		const { data, error } = await queries.getTransactionByTypes(types, courseId);
		if(error) {
			dispatch(Actions.setAlertSnackbar({
				state: true,
				type: "error",
				content: "Get course transaction fail"
			}));
		}
		if(data) return data
	}
)

export const getCourseLearnTracking = createAsyncThunk(
	'GET Course Learn Tracking',
	async ({ type, courseId }, { dispatch }) => {
		const { data, error } = await queries.queryTracking(type, courseId)
		if(error) {
			dispatch(Actions.setAlertSnackbar({
				state: true,
				type: "error",
				content: "Get course tracking fail"
			}));
		}
		if(data) {
			return data
		} else {
			const { data, error } = await mutations.saveTracking({
				type: trackingEvent.COURSE_LEARN,
				course: courseId,
				data: []
			})
			if(error) {
				dispatch(Actions.setAlertSnackbar({
					state: true,
					type: "error",
					content: "Create course tracking fail"
				}));
			}
			if (data) {
				return data
			}
		}
	}
)

export const updateCourseLearnTracking = createAsyncThunk(
	'Update Course Learn Tracking',
	async (data) => data
)