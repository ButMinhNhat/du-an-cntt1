import { createAction, createAsyncThunk } from "@reduxjs/toolkit";
import * as queries from 'graphql/query';

export const getCoursesList = createAsyncThunk(
	'GET Courses List',
	async (query) => {
		const result = queries.getCourses(query);
		return result;
	}
)

export const getCoursesInstructor = createAsyncThunk(
	'GET Courses Instructor List',
	async () => {
		const { data } = await queries.getInstructorCourses();
		if(data) {
			return data;
		}
	}
)

export const getMyLearning = createAsyncThunk(
	'GET My Learning Courses',
	async () => {
		const { data } = await queries.getMyLearningCourses();
		if(data) {
			return data;
		}
	}
)

export const clearCoursesListData = createAction('Clear Courses List Data')

export const getMyLearningTracking = createAsyncThunk(
	'GET My Learning Tracking',
	async ({ type, courseIds }) => {
		const { data } = await queries.queryTrackingForCourses(type, courseIds);
		if(data) {
			return data;
		}
	}
)