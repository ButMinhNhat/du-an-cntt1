import {
  Collapse,
  FormControl,
  IconButton,
  MenuItem,
  Select,
  TextField,
  Tooltip,
  Typography,
} from "@mui/material";
import React, { useEffect, useRef, useState } from "react";
import { makeStyles } from "@mui/styles";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import KeyboardArrowUpIcon from "@mui/icons-material/KeyboardArrowUp";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/Delete";
import CustomButton from "./CustomButton";
import CheckBoxIcon from "@mui/icons-material/CheckBox";
import RadioButtonCheckedIcon from "@mui/icons-material/RadioButtonChecked";
import CheckBoxOutlineBlankIcon from '@mui/icons-material/CheckBoxOutlineBlank';
import RadioButtonUncheckedIcon from '@mui/icons-material/RadioButtonUnchecked';
import TextSnippetIcon from "@mui/icons-material/TextSnippet";
import SaveIcon from "@mui/icons-material/Save";

const useStyles = makeStyles((theme) => ({
  hiddenContent: {
    "& .hide": {
      opacity: 0,
      visibility: "hidden",
      //   transition: theme.transitions.create("all", {
      //     duration: theme.transitions.duration.complex,
      //     easing: theme.transitions.easing.easeInOut,
      //   }),
    },
    "&:hover": {
      "& .hide": {
        opacity: 1,
        visibility: "visible",
      },
    },
  },
  hoverTriggerItem: {
    width: "100%",
    "& .hide": {
      opacity: 0,
      visibility: "hidden",
      // transition: theme.transitions.create('all', {
      // 	duration: theme.transitions.duration.complex,
      // 	easing: theme.transitions.easing.easeInOut
      // })
    },
    "&:hover": {
      "& .hide": {
        opacity: 1,
        visibility: "visible",
      },
    },
  },
}));

export const quizTypes = [
  {
    name: "Text",
    icon: <TextSnippetIcon />,
  },
  {
    name: "Radio",
    icon: <RadioButtonCheckedIcon />,
  },
  {
    name: "Checkbox",
    icon: <CheckBoxIcon />,
  },
];

//create new quiz question
const createNewQuestion = (type, questionTitle = "", optionTitle = "") => {
  //type 'TEXT'
  if (type === quizTypes[0].name) {
    return {
	  answers: [],
	  correctAnswers: [],
      title: questionTitle,
      type: type,
	  expand: true
    };
  }

  //type 'MULTI_CHOICE' or 'CHOICE'
  return {
    correctAnswers: [],
    title: questionTitle,
    answers: Array.from(
      { length: 3 },
      (_, i) => optionTitle + " " + parseInt(i + 1)
    ),
    type: type,
	expand: true
  };
};

const CourseQuiz = (props) => {
  const { quiz, idx, handleChange } = props;
  const classes = useStyles();
  const titleInputRef = useRef(null);

  const [isChangeTitle, setIsChangeTitle] = useState(!quiz.title);
  const [isExpand, setIsExpand] = useState(false);

  const handleDeleteLecture = () => {
    handleChange(idx, null);
  };

  const handleChangeLectureTitle = () => {
    if (!titleInputRef?.current) return;
    const { value } = titleInputRef?.current;
    handleChange(idx, {
      ...quiz,
      title: value,
    });
    setIsChangeTitle(false);
  };

  const addQuestion = (type) => {
    let updateQuestions = [...quiz.questions];
    //create new question type text
    const newQuestion = createNewQuestion(
      type,
      "Question title",
      "Option title"
    );

    updateQuestions.push(newQuestion);

    handleChange(idx, {
      ...quiz,
      questions: updateQuestions,
    });
  };

  const handleSaveQuizQuestion = (question, index) => {
    let updateQuestions = [...quiz.questions];
    if (index || index === 0) {
      updateQuestions[index] = question;
    } else {
      updateQuestions.push(question);
    }

    handleChange(idx, {
      ...quiz,
      questions: updateQuestions,
    });
  };

  const handleDeleteQuizQuestion = (index) => {
    let updateQuestions = [...quiz.questions];
    updateQuestions.splice(index, 1);
    handleChange(idx, {
      ...quiz,
      questions: updateQuestions,
    });
  };

  return (
    <div className="border-1 p-2 my-2">
      <div className="flex items-center">
        <div className={`${classes.hiddenContent} flex-1 flex items-center `}>
          <Typography className="font-bold text-md min-w-[120px]">
            {quiz.title}
          </Typography>
          <div className="flex-1 flex items-center">
            <div className="flex items-center ml-24">
              <Tooltip title="Edit title" placement="top" className="mx-2">
                <IconButton
                  onClick={() => {
                    setIsChangeTitle(true);
                    setIsExpand(true);
                  }}
                >
                  <EditIcon />
                </IconButton>
              </Tooltip>
              <Tooltip title="Delete section" placement="top" className="mx-2">
                <IconButton onClick={handleDeleteLecture}>
                  <DeleteIcon />
                </IconButton>
              </Tooltip>
            </div>
          </div>
          <div>
            <IconButton onClick={() => setIsExpand(!isExpand)}>
              {isExpand ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
            </IconButton>
          </div>
        </div>
      </div>
      {isChangeTitle && isExpand && (
        <div className="my-4 bg-white p-4 border-[1px] border-slate-700 rounded-sm">
          <TextField
		    autoComplete="off"
            placeholder="Section title"
            name="title"
            variant="outlined"
            inputRef={titleInputRef}
            defaultValue={quiz.title}
            fullWidth
            required
            autoFocus
          />
          <div className="my-4 flex justify-end">
            <CustomButton
              className="mr-8"
              variant="contained"
              onClick={() => setIsChangeTitle(false)}
            >
              Cancel
            </CustomButton>
            <CustomButton
              variant="contained"
              color="primary"
              onClick={handleChangeLectureTitle}
            >
              Save
            </CustomButton>
          </div>
        </div>
      )}
      <Collapse in={isExpand}>
        <div className="border-t-1 py-4">
          {quiz?.questions?.length > 0 && (
            <div>
              {quiz.questions.map((item, index) => (
                <QuestionItem
                  data={item}
                  key={index}
                  idx={index}
                  onSave={handleSaveQuizQuestion}
                  onDelete={handleDeleteQuizQuestion}
                />
              ))}
            </div>
          )}
          <div className="flex w-full items-center">
            {quizTypes.map((item, index) => (
              <CustomButton
                key={index}
                className="flex items-center cursor-pointer bg-white px-4 py-2 border-[1px] bg-gray-200 text-black hover:bg-gray-300 mr-4"
                onClick={() => addQuestion(item.name)}
              >
                {item.icon}
                <Typography className="ml-4">{item.name}</Typography>
              </CustomButton>
            ))}
          </div>
        </div>
      </Collapse>
    </div>
  );
};

const TextQuestion = (props) => {
  const { question, onChange } = props;

  const handleCorrectAnswer = (text) => {
    onChange({
      ...question,
      correctAnswers: text ? [text] : [], //if text is empty => delete correct answer
    });
  };

  return (
    <div>
      <TextField
	    autoComplete="off"
        placeholder="Type correct answer"
        variant="outlined"
        value={question.correctAnswers[0] || ""}
        fullWidth
        onChange={(ev) => handleCorrectAnswer(ev.target.value)}
        multiline
        rows={5}
        className="bg-white"
      />
    </div>
  );
};

const ChoiceQuestion = (props) => {
  const classes = useStyles();
  const { question, onChange, type } = props;
  const { answers, correctAnswers } = question;

  const handleChangeQuestion = (ans, correctAns) => {
    onChange({
      ...question,
      answers: ans,
      correctAnswers: correctAns,
    });
  };

  const handleChangeAnswer = (idx, ans) => {
    let updateAnswers = [...answers];
    updateAnswers[idx] = ans;
    //check if correct answer's name is change
    let updateCorrectAns = correctAnswers.map((item) => {
      if (item === answers[idx]) return ans;
      return item;
    });

    //get update answers
    handleChangeQuestion(updateAnswers, updateCorrectAns);
  };

  const handleAddOption = () => {
    const defaultTitle = "Option title " + parseInt(answers.length + 1);
    //check option title is exist
    const optionTitle = answers.includes(defaultTitle)
      ? `${defaultTitle + "*"}`
      : defaultTitle;

    const updateAnswers = [...answers, optionTitle];

    //get update answers
    handleChangeQuestion(updateAnswers, correctAnswers);
  };

  const handleRemoveOption = (idx) => {
    let updateAnswers = [...answers];
    updateAnswers.splice(idx, 1);
    //delete option from correct answer
    const updateCorrectAns = correctAnswers.filter(
      (item) => item !== answers[idx]
    );

    //get update answers
    handleChangeQuestion(updateAnswers, updateCorrectAns);
  };

  const handleChooseCorrectAnswer = (answer) => {
    if (type === quizTypes[1].name) {
      return handleChangeQuestion(answers, [answer]);
    }

    return toggleCorrectAnswer(answer);
  };

  const toggleCorrectAnswer = (answer) => {
    let updateCorrectAns = [...correctAnswers];
    if (correctAnswers.includes(answer)) {
      updateCorrectAns = correctAnswers.filter((ans) => ans !== answer);
    } else {
      updateCorrectAns = [...correctAnswers, answer];
    }

    //get update correct answers
    handleChangeQuestion(answers, updateCorrectAns);
  };

  return (
    <div>
      {answers.length > 0 && (
        <React.Fragment>
          {answers.map((ans, idx) => {
            const isCorrect = correctAnswers.includes(ans);
            return (
              <div key={"answer-" + idx} className={classes.hoverTriggerItem}>
                <div className="flex items-center mt-10">
                  <div>
                    <IconButton onClick={() => handleChooseCorrectAnswer(ans)}>
                      {isCorrect ? (
                        <>
                          {type === quizTypes[1].name ? <RadioButtonCheckedIcon /> : <CheckBoxIcon />}
                        </>
                      ) : (
                        <>
                          {type === quizTypes[1].name ? <RadioButtonUncheckedIcon /> : <CheckBoxOutlineBlankIcon /> }
                        </>
                      )}
                    </IconButton>
                  </div>
                  <TextField
				    autoComplete="off"
                    placeholder="Option title"
                    variant="outlined"
                    value={ans}
                    fullWidth
                    onChange={(ev) => handleChangeAnswer(idx, ev.target.value)}
                    className="bg-white flex-1"
                  />
                  <div className="hide">
                    <IconButton onClick={() => handleRemoveOption(idx)}>
                      <DeleteIcon />
                    </IconButton>
                  </div>
                </div>
              </div>
            );
          })}
        </React.Fragment>
      )}
      <CustomButton
        className="mt-8 flex cursor-pointer bg-white text-black hover:bg-white hover:border-[1px] hover:border-black border-[1px] border-white"
        onClick={handleAddOption}
      >
        <Typography>Add answer</Typography>
      </CustomButton>
    </div>
  );
};

const QuestionItem = (props) => {
  const { data, idx, onSave, onDelete } = props;

  const [question, setQuestion] = useState(data);
  const [isExpand, setIsExpand] = useState(data.expand ?? false);

  const handleChangeQuestion = (ev) => {
    const { name, value } = ev.target;
    setQuestion({
      ...question,
      [name]: value,
    });
  };

  const handleChangeType = (ev) => {
    const { value } = ev.target;
    if (value !== question.type) {
      const newQuestion = createNewQuestion(
        value,
        "Question title",
        "Option title"
      );
      setQuestion({
        ...question,
        type: value,
        answers: newQuestion.answers,
        correctAnswers: newQuestion.correctAnswers,
      });
    }
  };

  const handleChangeAnswer = (data) => {
    setQuestion(data);
  };

  const saveQuestion = () => {
    //check empty correct answer
    if (!question.title) {
      return false;
    }
    if (
      (question.answers?.length === 0 && question.type !== quizTypes[0].name) ||
      question.correctAnswers?.length === 0
    ) {
      return false;
    }
    onSave(question, idx);
	setIsExpand(false);
  };

  const deleteQuestion = () => {
    onDelete(idx);
  };

  useEffect(() => {
    setQuestion(data);
  }, [data]);

  return (
    <div className="bg-gray-200 p-4 mb-4">
      <div className="flex flex-col md:flex-row items-center">
        <div className="w-3/4 px-2">
          <TextField
		    autoComplete="off"
            placeholder="Question title"
            name="title"
            variant="outlined"
            value={question.title}
            fullWidth
            required
            onChange={handleChangeQuestion}
            onFocus={(ev) => ev.currentTarget.select()}
            className="bg-white"
          />
        </div>
        <div className="w-1/4 px-2">
          <FormControl fullWidth>
            <Select
              value={question.type}
              onChange={handleChangeType}
              className="bg-white"
            >
              {quizTypes.map((item, index) => (
                <MenuItem value={item.name} key={index}>
                  {item.icon} <span className="ml-2">{item.name}</span>
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        </div>
		<div>
		  <IconButton onClick={() => setIsExpand(!isExpand)} className="bg-white">
			{isExpand ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
		  </IconButton>
		</div>
      </div>
	  <Collapse in={isExpand}>
		<div className="mx-2 my-4">
			{question.type === quizTypes[0].name && (
			<TextQuestion question={question} onChange={handleChangeAnswer} />
			)}
			{(question.type === quizTypes[1].name ||
			question.type === quizTypes[2].name) && (
			<ChoiceQuestion
				question={question}
				type={question.type}
				onChange={handleChangeAnswer}
			/>
			)}
		</div>
		<div className="mx-2 my-4 text-right">
			<Tooltip title="Save question">
			<IconButton onClick={saveQuestion} className="ml-2">
				<SaveIcon />
			</IconButton>
			</Tooltip>
			<Tooltip title="Delete question">
			<IconButton onClick={deleteQuestion} className="ml-2">
				<DeleteIcon />
			</IconButton>
			</Tooltip>
		</div>
	  </Collapse>
    </div>
  );
};

export default CourseQuiz;
