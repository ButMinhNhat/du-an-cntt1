import React from "react";
import ReactPlayer from "react-player";

const VideoPlayer = (props) => {
  const { fileName } = props;

  return (
    <ReactPlayer
      url={`${process.env.REACT_APP_API_URL}/file/${fileName}`}
      controls
      width="100%"
      height="100%"
      playing={true}
    />
  );
};

export default VideoPlayer;
