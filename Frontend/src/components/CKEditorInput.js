import React from 'react';
import { CKEditor } from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import '../index.css';

const WysiwygInput = ({ value, onChange, onReady, onRawDataChange, name, ...props }) => {
    const hanldeChange = (_, editor) => {
        const data = editor.getData();
        onChange(data);
    }

    return (
        <CKEditor
            editor={ClassicEditor}
            data={value}
            onChange={hanldeChange}
            spellChek={false}
            {...props}
            config={{
                toolbar: {
                    items: [
                        'heading', '|',
                        'bold', 'italic', 'link', '|',
                        'outdent', 'indent', '|',
                        'bulletedList', 'numberedList', '|',
                        'insertTable', '|',
                        'uploadImage', 'blockQuote', '|',
                        'undo', 'redo'
                    ],
                    shouldNotGroupWhenFull: true
                },
            }}
            
        />
    )
}

WysiwygInput.defaultProps = {
    onChange: () => {},
}

export default React.memo(WysiwygInput);