import { IconButton, TextField, Typography } from "@mui/material";
import React from "react";
import CKEditorInput from "./CKEditorInput";
import CustomButton from "./CustomButton";
import Creatable from "react-select/creatable";
import Select from "react-select";
import { courseLevel } from "views/Instructor/CourseEdit";
import CustomUpload from "./CustomUpload";
import { useSelector } from "react-redux";
import ClearIcon from '@mui/icons-material/Clear';

export const courseDefaultThumbnail = `${process.env.PUBLIC_URL}/course_default_image.png`;

const CourseInformation = (props) => {
	const { data, handleChange } = props;

	const topics = useSelector(({ topic }) => topic.data);

	const generateSelect = (array) => {
		return array.map((item) => {
			return {
				value: item._id,
				label: item.name,
			};
		});
	};

	const getValue = (id) => {
		const dataTopic = topics.find((item) => item._id === id);
		return {
			value: dataTopic?._id,
			label: dataTopic?.name,
		};
	};

	return (
		<div>
			{/* course title */}
			<div className="mb-8">
				<div className="mb-2">
					<Typography variant="h6">
						<b>Title</b>
					</Typography>
				</div>
				<TextField
					autoComplete="off"
					placeholder="Enter the title of your course"
					name="title"
					variant="outlined"
					value={data.title}
					inputProps={{
						style: {
							padding: 12,
						},
					}}
					onChange={(e) => handleChange(e.target.value, e.target.name)}
					fullWidth
					required
				/>
			</div>

			{/* course introduction */}
			<div className="mb-8">
				<div className="mb-2">
					<Typography variant="h6">
						<b>Introduction</b>
					</Typography>
				</div>
				<TextField
					autoComplete="off"
					placeholder="Type your introduction"
					name="shortDescription"
					variant="outlined"
					value={data.shortDescription}
					inputProps={{
						style: {
							padding: 12,
						},
					}}
					onChange={(e) => handleChange(e.target.value, e.target.name)}
					fullWidth
					required
				/>
			</div>

			{/* course description */}
			<div className="mb-8">
				<div className="mb-2">
					<Typography variant="h6">
						<b>Description</b>
					</Typography>
				</div>
				<CKEditorInput
					value={data.description}
					name="description"
					onChange={(data) => handleChange(data, "description")}
				/>
			</div>

			{/* course cover image  */}
			<div className="mb-8">
				<div className="mb-2">
					<Typography variant="h6">
						<b>Cover image</b>
					</Typography>
				</div>
				<div className="flex flex-col md:flex-row">
					<div
						className="border-2 border-slate-400 mr-8"
						style={{
							width: "560px",
							height: "264px",
							// backgroundColor: "#E3E5ED",
							backgroundImage: `url(${data.coverImage
									? `${process.env.REACT_APP_API_URL}/file/${data.coverImage}`
									: courseDefaultThumbnail
								})`,
							backgroundPosition: "center",
							backgroundSize: "cover",
							backgroundRepeat: "no-repeat",
						}}
					/>
					<div className="flex-1">
						<div className="text-lg">
							Upload your course image here. Important guidelines: 750x422
							pixels; format .jpg, .jpeg,. gif, or .png; no text on the image.
						</div>
						<CustomUpload
							handleUpload={(file) => handleChange(file, "coverImage")}
							accept="image/png,image/gif,image/jpeg"
						>
							<CustomButton className="mt-4">Upload Image</CustomButton>
						</CustomUpload>
					</div>
				</div>
			</div>

			{/* course topic */}
			<div className="mb-8">
				<div className="mb-2">
					<Typography variant="h6">
						<b>Topics</b>
					</Typography>
				</div>
				{topics && (
					<div className="flex flex-col md:flex-row space-x-8">
						<Select
							value={data.topic[0] ? getValue(data.topic[0]) : null}
							isClearable
							className="flex-1"
							placeholder="Select category"
							options={generateSelect(topics.filter((item) => !item.parent))}
							onChange={(select) => {
								if (!select) {
									handleChange([], "topic");
								} else {
									handleChange([select.value], "topic");
								}
							}}
						/>
						<Select
							value={data.topic[1] ? getValue(data.topic[1]) : null}
							isClearable
							className="flex-1"
							placeholder="Select sub category"
							options={
								data.topic[0]
									? generateSelect(topics.filter((item) => item.parent?._id === data.topic[0]))
									: []
							}
							onChange={(select) => {
								if (!select) {
									handleChange([data.topic[0]], "topic");
								} else {
									handleChange([data.topic[0], select.value], "topic");
								}
							}}
						/>
						<Select
							value={data.topic[2] ? getValue(data.topic[2]) : null}
							isClearable
							className="flex-1"
							placeholder="Select topic"
							options={
								data.topic[1]
									? generateSelect(topics.filter((item) => item.parent?._id === data.topic[1]))
									: []
							}
							onChange={(select) => {
								if (!select) {
									handleChange([data.topic[0], data.topic[1]], "topic");
								} else {
									handleChange([data.topic[0], data.topic[1], select.value], "topic");
								}
							}}
						/>
					</div>
				)}
			</div>

			{/* course level and hastag */}
			<div className="mb-8 flex flex-col md:flex-row">
				<div className="mr-8">
					<div className="mb-2">
						<Typography variant="h6">
							<b>Level</b>
						</Typography>
					</div>
					{courseLevel.map((item, index) => (
						<CustomButton
							key={index}
							variant="contained"
							className={`mr-8 normal-case rounded-lg ${item !== data.level
									? "bg-stone-300 text-black hover:text-white"
									: ""
								}`}
							onClick={() => handleChange(item, "level")}
						>
							{item}
						</CustomButton>
					))}
				</div>
				<div className="flex-1">
					<div className="mb-2">
						<Typography variant="h6">
							<b>Hastags</b>
						</Typography>
					</div>
					<Creatable
						isMulti
						value={data.hashtags}
						placeholder="Create #hastag for your course!"
						onChange={(value) => handleChange(value, "hashtags")}
					/>
				</div>
			</div>

			{/* course features */}
			<div className="mb-8">
				<div className="mb-2">
					<Typography variant="h6">
						<b>Features</b>
					</Typography>
				</div>
				{data.features.map((item, index) => (
					<div key={index} className="flex items-center mb-4">
						<TextField
							autoComplete="off"
							className="flex-1 mr-2"
							variant="outlined"
							value={item}
							inputProps={{
								style: {
									padding: 12,
								},
							}}
							onChange={(ev) => {
								const newData = [...data.features];
								newData[index] = ev.target.value;
								handleChange(newData, "features");
							}}
							fullWidth
							placeholder="Course feature"
						/>
						<IconButton 
							className="opacity-0 hover:opacity-100" 
							onClick={() => handleChange([...data.features].filter(f => f !== item), "features")}
						>
							<ClearIcon />
						</IconButton>
					</div>
				))}
				<CustomButton
					className="font-bold flex items-center normal-case rounded-lg"
					onClick={() => handleChange([...data.features, ""], "features")}
				>
					Add more feature
				</CustomButton>
			</div>
		</div>
	);
};

export default CourseInformation;
