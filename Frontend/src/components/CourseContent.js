import {
  Collapse,
  Divider,
  IconButton,
  TextField,
  Tooltip,
  Typography,
} from "@mui/material";
import React, { useEffect, useRef, useState } from "react";
import AddCircleOutlineIcon from "@mui/icons-material/AddCircleOutline";
import { makeStyles } from "@mui/styles";
import CustomButton from "./CustomButton";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import KeyboardArrowUpIcon from "@mui/icons-material/KeyboardArrowUp";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/Delete";
import _ from "lodash";
import { isLecture, isQuiz } from "ultis/course";
import CourseLecture from "./CourseLecture";
import CourseQuiz from "./CourseQuiz";

const useStyles = makeStyles((theme) => ({
  hiddenContent: {
    "& .hide": {
      opacity: 0,
      visibility: "hidden",
      //   transition: theme.transitions.create("all", {
      //     duration: theme.transitions.duration.complex,
      //     easing: theme.transitions.easing.easeInOut,
      //   }),
    },
    "&:hover": {
      "& .hide": {
        opacity: 1,
        visibility: "visible",
      },
    },
  },
}));

export const lectureShape = {
  title: "Untitled lecture",
  files: [],
};

export const quizShape = {
  title: "Untitled quiz",
  questions: [],
};

export const sectionShape = {
  title: "Untitled section",
  quiz: [],
  lectures: [],
};

const CourseContent = (props) => {
  const { data, handleChange } = props;
  const classes = useStyles();

  const addCourseSection = (addIdx) => {
    let udpateSections = [...data.sections];

    udpateSections.splice(addIdx, 0, sectionShape);

    handleChange(udpateSections, "sections");
  };

  const handleChangeSection = (sectionIndex, value) => {
    let udpateSections = [...data.sections];
    if (!value) {
      //remove section
      udpateSections.splice(sectionIndex, 1);
    } else {
      udpateSections[sectionIndex] = value;
    }

    handleChange(udpateSections, "sections");
  };

  const getSequenceNum = (sectionIdx) => {
    let num = 0;
    for (let i = 0; i < sectionIdx; i++) {
      let items = _.sortBy(
        [
          ...(data.sections[i]?.quiz || []),
          ...(data.sections[i]?.lectures || []),
        ],
        "sequence"
      );
      num += items.length;
    }
    return num;
  };

  return (
    <div>
      {data.sections.map((section, index) => (
        <React.Fragment key={"section" + index}>
          <div className={`${classes.hiddenContent} py-[10px] `}>
            <div className="hide flex items-center">
              <Tooltip
                title="Add section"
                className="cursor-pointer"
                onClick={() => addCourseSection(index)}
              >
                <AddCircleOutlineIcon />
              </Tooltip>
              <Divider className="ml-5 flex-1" />
            </div>
          </div>

          <CourseSection
            idx={index}
            section={section}
            handleChangeSection={handleChangeSection}
			sequenceStart={getSequenceNum(index)}
          />
        </React.Fragment>
      ))}
      <CustomButton
        className="py-2 px-4 cursor-pointer flex items-center w-40 mt-8"
        onClick={() => addCourseSection(data?.sections?.length)}
      >
        <AddCircleOutlineIcon />
        <Typography className="ml-4">Add section</Typography>
      </CustomButton>
    </div>
  );
};

const CourseSection = (props) => {
  const { idx, section, sequenceStart, handleChangeSection } = props;
  const classes = useStyles();
  const titleInputRef = useRef(null);

  const [sectionItems, setSectionItems] = useState(_.sortBy([...section.lectures || [], ...section.quiz || []], 'sequence'));
  const [isChangeTitle, setIsChangeTitle] = useState(!section.title);
  const [isExpand, setIsExpand] = useState(false);

  const handleDeleteSection = () => {
    handleChangeSection(idx, null);
  };

  const handleChangeSectionTitle = () => {
    const { name, value } = titleInputRef.current;

    handleChangeSection(idx, {
      ...section,
      [name]: value,
    });

    setIsChangeTitle(false);
  };

  const indexSectionItem = (items) => {
	return items.map((i, idx) => ({
		...i,
		sequence: sequenceStart + idx + 1
	}))
  }

  const addLecture = (index) => {
    const sectionItemsUpdate = [...sectionItems];
    sectionItemsUpdate.splice(index, 0, lectureShape);

    handleChangeSection(idx, {
      ...section,
      quiz: indexSectionItem(sectionItemsUpdate).filter((i) => isQuiz(i)),
      lectures: indexSectionItem(sectionItemsUpdate).filter((i) => isLecture(i)),
    });
  };

  const addQuiz = async (index) => {
    let sectionItemsUpdate = [...sectionItems];
    sectionItemsUpdate.splice(index, 0, quizShape);

    handleChangeSection(idx, {
      ...section,
      quiz: indexSectionItem(sectionItemsUpdate).filter((i) => isQuiz(i)),
      lectures: indexSectionItem(sectionItemsUpdate).filter((i) => isLecture(i)),
    });
  };

  const handleSectionItemChange = (index, value) => {
	let itemsUpdate = [...sectionItems];

	if (!value) {
		itemsUpdate.splice(index, 1)
	}
	else {
		itemsUpdate[index] = value;
	}

	itemsUpdate = indexSectionItem(itemsUpdate)

	const sectionUpdate = {
		...section,
		quiz: indexSectionItem(itemsUpdate).filter(i => isQuiz(i)),
		lectures: indexSectionItem(itemsUpdate).filter(i => isLecture(i))
	}

	handleChangeSection(idx, sectionUpdate)
  }

  useEffect(() => {
	setSectionItems(_.sortBy([...section.lectures || [], ...section.quiz || []], 'sequence'));
  }, [section.lectures, section.quiz]);

  return (
    <div className="px-4 py-8 border-[1px] rounded-sm bg-gray-100 border-slate-700">
      <div className="flex items-center divide-y">
        <div className={`${classes.hiddenContent} flex-1 flex items-center `}>
          <Typography className="font-bold text-lg min-w-[140px]">
            {section.title}
          </Typography>
          <div className="flex-1 flex items-center">
            <div className="flex items-center ml-24">
              <Tooltip title="Edit title" placement="top" className="mx-2">
                <IconButton
                  onClick={() => {
                    setIsChangeTitle(true);
                    setIsExpand(true);
                  }}
                >
                  <EditIcon />
                </IconButton>
              </Tooltip>
              <Tooltip title="Delete section" placement="top" className="mx-2">
                <IconButton onClick={handleDeleteSection}>
                  <DeleteIcon />
                </IconButton>
              </Tooltip>
            </div>
          </div>
          <div>
            <IconButton onClick={() => setIsExpand(!isExpand)}>
              {isExpand ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
            </IconButton>
          </div>
        </div>
      </div>
      {isChangeTitle && isExpand && (
        <div className="my-4 bg-white p-4 border-[1px] border-slate-700 rounded-sm">
          <TextField
		    autoComplete="off"
            placeholder="Section title"
            name="title"
            variant="outlined"
            inputRef={titleInputRef}
            defaultValue={section.title}
            fullWidth
            required
            autoFocus
          />
          <div className="my-4 flex justify-end">
            <CustomButton
              className="mr-8"
              variant="contained"
              onClick={() => setIsChangeTitle(false)}
            >
              Cancel
            </CustomButton>
            <CustomButton
              variant="contained"
              color="primary"
              onClick={handleChangeSectionTitle}
            >
              Save
            </CustomButton>
          </div>
        </div>
      )}
      <Collapse in={isExpand}>
		<div>
			{sectionItems.map((item, idx) => (
				<div className="bg-white p-2 my-4 border-[1px] border-slate-700 rounded-sm" key={idx}>
					{isLecture(item) &&
						<CourseLecture
							lecture={item}
							idx={idx}
							handleChange={handleSectionItemChange}
						/>
					}
					{isQuiz(item) &&
						<CourseQuiz
							quiz={item}
							idx={idx}
							handleChange={handleSectionItemChange}
						/>
					}
				</div>
			))}
		</div>
        <div className="flex my-4">
          <CustomButton
            className="flex items-center cursor-pointer bg-white px-4 py-2 border-[1px] border-slate-700 text-black hover:bg-white"
            onClick={() => addLecture(sectionItems.length)}
          >
            <AddCircleOutlineIcon />
            <Typography className="ml-4">Add article</Typography>
          </CustomButton>
          <CustomButton
            className="flex items-center ml-12 cursor-pointer bg-white px-4 py-2 border-[1px] border-slate-700 text-black hover:bg-white"
            onClick={() => addQuiz(sectionItems.length)}
          >
            <AddCircleOutlineIcon />
            <Typography className="ml-4">Add quiz</Typography>
          </CustomButton>
        </div>
      </Collapse>
    </div>
  );
};

export default CourseContent;
