import React from "react";
import { countAvg, countPercent } from "views/Course Page/CourseDetailPage";
import StarRating from "./StarRating";

const CourseRatingStatistics = (props) => {
	const { data } = props;

	if(!Array.isArray(data)) return;

	return (
		<div className="mt-4">
			<div className="flex flex-col md:flex-row items-center">
				<div className="flex flex-col items-center">
					<div className="text-yellow-500 text-7xl mb-2 font-bold">{countAvg(data)}</div>
					<StarRating star={countAvg(data)} size={20} />
					<div className="text-yellow-500 text-md font-bold">Course grade</div>
				</div>
				<div className="flex flex-col flex-1">
					<StarStatistic star={5} percentage={countPercent(data, 5)} />
					<StarStatistic star={4} percentage={countPercent(data, 4)} />
					<StarStatistic star={3} percentage={countPercent(data, 3)} />
					<StarStatistic star={2} percentage={countPercent(data, 2)} />
					<StarStatistic star={1} percentage={countPercent(data, 1)} />
				</div>
			</div>
		</div>
	);
};

const StarStatistic = ({ star, percentage }) => {
	return (
		<div className="flex items-center ml-8">
			<div className="mx-4 w-2/4 h-2 bg-gray-200 rounded dark:bg-gray-700">
				<div
					className="h-2 bg-yellow-400 rounded"
					style={{ width: `${percentage}%` }}
				></div>
			</div>
			<div className="ml-2 sm:ml-10 flex items-center">
				<StarRating star={star} size={20} />
				<div className="ml-2">{percentage}%</div>
			</div>
		</div>
	);
};

export default CourseRatingStatistics;
