import { TextField } from "@mui/material";
import moment from "moment";
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { transactionEvent } from "ultis/Common";
import UserAvatar from "./UserAvatar";
import * as mutations from "graphql/mutation";
import * as Actions from "actions";

const CourseDiscuss = () => {
  const dispatch = useDispatch();

  const { COURSE_COMMENT, COURSE_RATING } = transactionEvent;
  const [input, setInput] = useState("");

  const currentUser = useSelector(({ auth }) => auth.data);
  const transactions = useSelector(({ course }) => course.transactions);
  const course = useSelector(({ course }) => course.data);

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const inputProp = {
        type: COURSE_COMMENT,
        course: course._id,
        content: {
          text: input,
        },
      };
      const { data, error } = await mutations.createTransaction(inputProp);
      if (error) return console.log(error);
      if (data) {
        setInput("");
        return dispatch(
          Actions.getCourseTransactions({
            types: [COURSE_COMMENT, COURSE_RATING],
            courseId: course._id,
          })
        );
      }
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <React.Fragment>
      <div className="flex py-4 border-b-[1px] border-gray-300">
        <UserAvatar url={currentUser.avatar ?? ""} className="mr-4" />
        <form onSubmit={handleSubmit} className="flex-1">
          <TextField
            autoComplete="off"
            fullWidth
            placeholder="Type your discussion..."
            value={input}
            onChange={(e) => setInput(e.target.value)}
          />
        </form>
      </div>
      {transactions && (
        <React.Fragment>
          {transactions
            .filter((item) => item.type === transactionEvent.COURSE_COMMENT)
            .reverse()
            .map((item, index) => (
              <div
                className="flex py-4 border-b-[1px] border-gray-300 flex-start"
                key={index}
              >
                <UserAvatar
                  url={item.createdBy?.avatar ?? ""}
                  className="mr-4"
                />
                <div className="flex-1">
                  <div>
                    <span className="text-xl font-bold">
                      {item.createdBy?.name}
                    </span>{" "}
                    -{" "}
                    <span className="font-italic">
                      {moment(parseInt(item.createdAt)).format("DD/MM/YYYY")}
                    </span>
                  </div>
                  <div className="text-lg mt-2">{item.content?.text}</div>
                </div>
              </div>
            ))}
        </React.Fragment>
      )}
    </React.Fragment>
  );
};

export default CourseDiscuss;
