import moment from "moment";
import React from "react";
import StarRating from "./StarRating";
import UserAvatar from "./UserAvatar";

const CourseRatingList = (props) => {
  const { data } = props;

  if (!Array.isArray(data)) return;

  return (
    <div>
      {data.map((item, index) => (
        <div
          className="flex py-4 border-b-[1px] border-gray-300 flex-start"
          key={index}
        >
          <UserAvatar url={item.createdBy?.avatar ?? ""} className="mr-4" />
          <div className="flex-1">
			<div className="text-xl font-bold">{item.createdBy?.name}</div>
			<div className="flex my-2 items-center">
				<StarRating star={item.content?.value} />
				<span className="text-sm ml-2 text-slate-600">
					{moment(parseInt(item.createdAt)).format("DD/MM/YYYY")}
				</span>
			</div>
            <div className="text-lg mt-2">{item.content?.text}</div>
          </div>
        </div>
      ))}
    </div>
  );
};

export default CourseRatingList;
