import { Checkbox, Collapse, Dialog, Typography } from "@mui/material";
import React, { useState } from "react";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import KeyboardArrowUpIcon from "@mui/icons-material/KeyboardArrowUp";
import PlayCircleOutlineIcon from "@mui/icons-material/PlayCircleOutline";
import DescriptionOutlinedIcon from "@mui/icons-material/DescriptionOutlined";
import RatingBox from "./RatingBox";
import CustomButton from "./CustomButton";
import { useDispatch, useSelector } from "react-redux";
import { transactionEvent } from "ultis/Common";
import HelpIcon from '@mui/icons-material/Help';
import * as Actions from 'actions';

const CourseContentView = (props) => {
	const { data, onChangeLecture } = props;
	const { COURSE_RATING } = transactionEvent;

	const [openDialog, setOpenDialog] = useState(false);
	const transactions = useSelector(({ course }) => course.transactions);

	const handleCloseDialog = () => {
		setOpenDialog(false);
	};

	const canRate = () => {
		return !(transactions?.filter((item) => item.type === COURSE_RATING)?.length > 0)
	}

	return (
		<div className="overflow-y-auto">
			<div className="font-bold p-4 text-lg border-[1px] border-slate-400 flex">
				<span className="flex-1">Course content</span>
				{canRate() && (
						<CustomButton onClick={() => setOpenDialog(true)}>
							Assign a rating
						</CustomButton>
					)}
			</div>
			<div className="border-l-[1px] border-r-[1px] border-slate-400">
				{data.map((item, index) => (
					<SectionView
						data={item}
						key={index}
						index={index}
						onChangeLecture={onChangeLecture}
					/>
				))}
			</div>
			{canRate() && (
			<Dialog open={openDialog} onClose={handleCloseDialog}>
				<RatingBox onClose={handleCloseDialog} />
			</Dialog>
			)}
		</div>
	);
};

const SectionView = (props) => {
	const { data, index, onChangeLecture } = props;
	const dispatch = useDispatch();

	const tracking = useSelector(({ course }) => course.tracking);
	const [expand, setExpand] = useState(false);

	const handleCheckSesssion = (e, id) => {
		let updateData = [...tracking.data];
		if(e.target.checked){
			updateData.push(id);
		} else {
			updateData = updateData.filter(item => item !== id);
		}
		dispatch(Actions.updateCourseLearnTracking({
			...tracking,
			data: updateData
		}))
	}

	return (
		<div>
			<div className="bg-gray-100 p-4 border-b-[1px] border-slate-400">
				<div
					className="flex items-start cursor-pointer"
					onClick={() => setExpand(!expand)}
				>
					<Typography className="flex-1 font-semibold text-md">
						Section {index + 1}: {data.title}
					</Typography>
					{expand ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
				</div>
				<div className="flex">
					<Typography className="text-sm">
						{data?.lectures?.filter(item => tracking?.data?.includes(item._id))?.length} / {data.lectures?.length}
					</Typography>
				</div>
			</div>
			<Collapse in={expand} className="border-b-[1px] border-slate-400">
				{Array.isArray(data.lectures) && (
					<div className="flex flex-col">
						{data.lectures.map((item, idx) => (
							<div
								key={idx}
								className="px-4 py-2 hover:bg-slate-300 cursor-pointer"
							>
								<div className="flex items-start">
									<Checkbox
										className="mr-[10px] p-0"
										checked={tracking?.data?.includes(item._id) ?? false}
										onChange={(e) => handleCheckSesssion(e, item._id)}
										disabled={!tracking?.data}
										sx={{
											color: "black",
											"&.Mui-checked": {
												color: "black",
											},
										}}
									/>
									<div
										className="flex-1"
										onClick={() => onChangeLecture(item.sequence - 1 ?? 0)}
									>
										<div>
											{item.sequence}. {item.title}
										</div>
										<div className="-ml-[4px]">
											{item.media ? (
												<PlayCircleOutlineIcon className="h-[20px] w-[20px]" />
											) : (
												<DescriptionOutlinedIcon className="h-[20px] w-[20px]" />
											)}
										</div>
									</div>
								</div>
							</div>
						))}
						{data.quiz.map((item, idx) => (
							<div
								key={idx}
								className="px-4 py-2 hover:bg-slate-300 cursor-pointer"
							>
								<div className="flex items-start">
									<div
										className="flex-1 flex items-center py-2"
										onClick={() => onChangeLecture(item.sequence - 1 ?? 0)}
									>
										<HelpIcon className="h-[20px] w-[20px] ml-[2px]" />
										<div className="ml-4">
											{item.title}
										</div>
									</div>
								</div>
							</div>
						))}
					</div>
				)}
			</Collapse>
		</div>
	);
};

export default CourseContentView;
