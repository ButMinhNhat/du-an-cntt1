import React, { useState } from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";

const CategoryListView = (props) => {
  const topics = useSelector(({ topic }) => topic.data);

  const [category, setCategory] = useState(null);
  const [subCategory, setSubCategory] = useState(null);

  if (!topics) return null;

  return (
    <div
      className="border-[1px] border-slate-300 bg-white flex flex-row text-black my-4"
      style={{ height: props.height }}
	  onMouseLeave={() => {
		setCategory(null);
		setSubCategory(null);
	  }}
    >
      <div className="w-[260px] px-4 py-2">
        {topics
          .filter((item) => !item.parent)
          .map((item, index) => (
            <Link
              className="flex items-center hover:text-indigo-600 my-4"
              to={`/courses?topic=${item.slug}`}
              key={index}
              onMouseOver={() => {
				setCategory(item._id);
				setSubCategory(null);
			  }}
            >
              <div className="flex-1">{item.name}</div>
              <ArrowIcon />
            </Link>
          ))}
      </div>
      {category && (
        <div className="w-[260px] px-4 py-2 border-l-[1px] border-slate-300">
          {topics
            .filter((item) => item.parent?._id === category)
            .map((item, index) => (
              <Link
                className="flex items-center hover:text-indigo-600 my-[20px]"
                to={`/courses?topic=${item.slug}`}
                key={index}
                onMouseOver={() => setSubCategory(item._id)}
              >
                <div className="flex-1">{item.name}</div>
                <ArrowIcon />
              </Link>
            ))}
        </div>
      )}
	  {subCategory && (
        <div className="w-[260px] px-4 py-2 border-l-[1px] border-slate-300">
          {topics
            .filter((item) => item.parent?._id === subCategory)
            .map((item, index) => (
              <Link
                className="flex items-center hover:text-indigo-600 my-[20px]"
                to={`/courses?topic=${item.slug}`}
                key={index}
              >
                <div className="flex-1">{item.name}</div>
                <ArrowIcon />
              </Link>
            ))}
        </div>
      )}
    </div>
  );
};

const ArrowIcon = () => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="24"
      height="24"
      fill="currentColor"
      className="bi bi-arrow-right-short"
      viewBox="0 0 16 16"
    >
      <path
        fillRule="evenodd"
        d="M4 8a.5.5 0 0 1 .5-.5h5.793L8.146 5.354a.5.5 0 1 1 .708-.708l3 3a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708-.708L10.293 8.5H4.5A.5.5 0 0 1 4 8z"
      />
    </svg>
  );
};

export default CategoryListView;
