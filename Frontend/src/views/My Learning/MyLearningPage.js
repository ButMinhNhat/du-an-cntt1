import React, { useEffect, useState } from "react";
import HomeLayout from "layouts/Home/HomeLayout";
import { useDispatch, useSelector } from "react-redux";
import * as Actions from "actions";
import { Link } from "react-router-dom";
import { courseDefaultThumbnail } from "components/CourseInformation";
import { trackingEvent } from "ultis/Common";
import { getCourseLearningListWithoutQuiz } from "views/Course Page/CourseLearnPage";

const MyLearningPage = () => {
  const dispatch = useDispatch();

  const courses = useSelector(({ courses }) => courses.data);

  useEffect(() => {
    dispatch(Actions.getMyLearning());

    return () => {
      dispatch(Actions.clearCoursesListData());
    };
  }, [dispatch]);

  useEffect(() => {
    if (Array.isArray(courses)) {
      dispatch(
        Actions.getMyLearningTracking({
          type: trackingEvent.COURSE_LEARN,
          courseIds: courses.map((item) => item._id),
        })
      );
    }
  }, [dispatch, courses]);

  if (!courses) return <HomeLayout />;

  return (
    <HomeLayout>
      <div className="flex pb-4 mb-4 items-center border-b-[1px] border-slate-300">
        <div className="text-3xl font-bold flex-1">My Learning</div>
      </div>
      <div className="grid grid-cols-4 gap-10">
        {courses.map((item, index) => (
          <CourseItem key={index} data={item} />
        ))}
      </div>
    </HomeLayout>
  );
};

const CourseItem = (props) => {
  const { data } = props;

  const [processValue, setProcessValue] = useState(0);
  const tracking = useSelector(({ courses }) => courses.tracking);

  useEffect(() => {
	if(tracking) {
		const courseTracking = tracking.find(item => item.course?._id === data._id);
		if(courseTracking) {
			const courseSectionIds = getCourseLearningListWithoutQuiz(data)?.map(item => item._id);
			const trackingSectionIds = courseTracking.data?.filter(item => courseSectionIds.includes(item));
			const result = trackingSectionIds.length / courseSectionIds.length * 100;

			if(!Number.isNaN(result)) setProcessValue(result);
		}
	}
  }, [tracking, data])

  if (!data) return;

  return (
    <div className="w-full bg-white border-[1px] border-slate-300">
      <Link to={`/learn/${data.slug}`}>
        <img
          className="w-full h-[180px]"
          src={
            data.coverImage
              ? `${process.env.REACT_APP_API_URL}/file/${data.coverImage}`
              : courseDefaultThumbnail
          }
          alt="course thumbnail"
        />
      </Link>
      <div className="p-2">
        <Link
          to={`/learn/${data.slug}`}
          className="my-2 font-bold text-xl line-clamp-2 h-[56px]"
        >
          {data.title}
        </Link>
      </div>
      <div className="px-2 mb-4">
        <div className="w-full h-[5px] bg-gray-200 rounded dark:bg-gray-700">
          <div
            className="h-[5px] bg-violet-700 rounded"
            style={{ width: `${processValue}%` }}
          ></div>
        </div>
      </div>
    </div>
  );
};

export default MyLearningPage;
