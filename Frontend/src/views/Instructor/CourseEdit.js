import React, { useCallback, useEffect, useState } from "react";
import HomeLayout from "layouts/Home/HomeLayout";
import CourseInformation from "components/CourseInformation";
import { useDispatch, useSelector } from "react-redux";
import * as Actions from "actions";
import { Navigate, useNavigate, useParams } from "react-router-dom";
import CourseContent, { sectionShape } from "components/CourseContent";
import * as mutations from "graphql/mutation";
import _ from "lodash";

export const courseLevel = ["Easy", "Normal", "Hard"];
export const courseStatus = ["Public", "Share", "Private"];

const courseInitData = {
  title: "",
  status: courseStatus[0],
  share: [],
  shortDescription: "",
  description: "",
  coverImg: "",
  features: [],
  level: courseLevel[0],
  topic: [],
  hashtags: [],
  sections: [sectionShape],
};

const CourseEdit = () => {
  const params = useParams();
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { slug } = params;

  const courseData = useSelector(({ course }) => course.data);
  const isLogin = useSelector(({ auth }) => auth.login);

  const [data, setData] = useState(courseData);
  const [view, setView] = useState(0);

  const isCreateCourse = useCallback(() => {
	return slug === "new";
  }, [slug])

  const handleChange = (value, type) => {
    setData({
      ...data,
      [type]: value,
    });
  };

  const isValidCourse = () => {
    return data.title && data.sections?.length > 0;
  };

  const updateCourseQuiz = async (course) => {
    let newSections = [],
      terLoop = false;
    if (!course?.sections) {
      return course;
    }
    for (let section of course.sections) {
      if (terLoop) {
        console.log("Create course quiz fail");
        break;
      }
      let sectionQuiz = section.quiz;
      let newSectionQuiz = [];
      if (sectionQuiz?.length > 0) {
        for (let quiz of sectionQuiz) {
          let result, err;
          const { data, error } = await mutations.createQuiz(quiz);
          if (error) {
            err = error;
          }
          result = data;

          if (err) {
            terLoop = true;
            break;
          }
          newSectionQuiz.push(result._id);
        }
      }

      newSections.push({
        ...section,
        quiz: newSectionQuiz,
      });
    }

    return {
      ...course,
      sections: newSections,
    };
  };

  const handleSaveCourse = async () => {
    try {
      let courseBody = _.pickBy(data, (field) => !_.isEmpty(field));

      if (courseBody.sections) {
        courseBody.sections.map((section) => {
          if (section.lectures) {
            section.lectures.map((lecture) => {
			  delete lecture.type;
              return delete lecture.expand;
            });
          }
          if (section.quiz) {
            section.quiz.map((quiz) => {
			  if(quiz.questions) {
				quiz.questions.map(q => {
					return delete q.expand
				})
			  }
              return delete quiz.expand;
            });
          }
          return section;
        });
      }

      if (
        Array.isArray(courseBody.hashtags) &&
        courseBody.hashtags.length > 0
      ) {
        const hashtags = courseBody.hashtags.map((item) => item.value);
        courseBody.hashtags = hashtags;
      }
	  courseBody.attendees = courseBody.attendees?.map(item => item._id);

      delete courseBody.createdAt;
	  delete courseBody.updatedAt;
	  delete courseBody.createdBy;
	  delete courseBody.slug;
      courseBody = await updateCourseQuiz(courseBody);

      //save course
      const { error } = await mutations.saveCourse(courseBody);
      if (error) return console.log(error);
      navigate("/instructor");
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    if (slug) {
      if (isCreateCourse()) {
        dispatch(Actions.createNewCourse(courseInitData));
      } else {
        dispatch(Actions.getCoursesInstructorBySlug(slug));
      }
    }

	return () => {
		dispatch(Actions.clearCourseData());
	}
  }, [dispatch, slug, isCreateCourse]);

  useEffect(() => {
    if (courseData) {
      if (isCreateCourse()) {
        setData(courseData);
      } else {
        setData({
          ...courseData,
          topic: courseData.topic?.map((item) => item._id) ?? [],
		  hashtags: courseData.hashtags?.map((item) => {
			return {
		       label: item,
			   value: item
			}
		  }) ?? []
        });
      }
    }
  }, [courseData, slug, isCreateCourse]);

  if(!isLogin) return <Navigate to={`/auth/signin`}/>

  if (!data) return <HomeLayout></HomeLayout>;

  return (
    <HomeLayout>
      <div className="flex flex-col md:flex-row">
        <div className="w-[260px] mr-[24px]">
          <div className="font-bold text-2xl mb-16">{isCreateCourse() ? "Create Course" : "Update Course"}</div>
          <div
            className={`cursor-pointer border-black border-2 p-4 font-semibold mb-8 rounded-xl 
						${view === 0 ? "text-white bg-black" : "text-black bg-white"}`}
            onClick={() => setView(0)}
          >
            Course Information
          </div>
          <div
            className={`cursor-pointer border-black border-2 p-4 font-semibold mb-8 rounded-xl 
						${view === 1 ? "text-white bg-black" : "text-black bg-white"}`}
            onClick={() => setView(1)}
          >
            Course Content
          </div>
          <div className="border-t-2 mb-8 border-slate-400" />
          <div
            className={`cursor-pointer px-4 py-2 font-semibold mb-8 text-center 
		  	${
          isValidCourse()
            ? "bg-indigo-600 text-white hover:bg-indigo-700"
            : "bg-stone-300 text-black"
        }
		  `}
            onClick={!isValidCourse() ? () => {} : () => handleSaveCourse()}
          >
            {isCreateCourse() ? "PUBLISH" : "SAVE" }
          </div>
        </div>
        <div className="shadow-xl flex-1 px-[48px] pb-[48px]">
          {view === 0 && (
            <CourseInformation data={data} handleChange={handleChange} />
          )}
          {view === 1 && (
            <CourseContent data={data} handleChange={handleChange} />
          )}
        </div>
      </div>
    </HomeLayout>
  );
};

export default CourseEdit;
