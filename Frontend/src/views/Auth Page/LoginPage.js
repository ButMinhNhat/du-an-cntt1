import React, { useState } from "react";
import HomeLayout from "layouts/Home/HomeLayout";
import { TextField } from "@mui/material";
import CustomButton from "components/CustomButton";
import { useDispatch, useSelector } from "react-redux";
import * as Actions from "actions";
import { Navigate, useNavigate } from "react-router-dom";
import * as mutations from "graphql/mutation";

const LoginPage = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const isLogin = useSelector(({ auth }) => auth.login);

  const [data, setData] = useState({
    username: "",
    password: "",
  });

  const handleChange = (e) => {
    setData({
      ...data,
      [e.target.name]: e.target.value,
    });
  };

  const handleSubmit = async (e) => {
	e.preventDefault();
    if (data.username && data.password) {
      try {
        const result = await mutations.login(data);
		if(result) {
			dispatch(Actions.loginAccount(result));
			navigate(-1);
		}
      } catch (error) {
        console.log(error);
      }
    }
  };

  if(isLogin) return <Navigate to={`/`}/>

  return (
    <HomeLayout>
      <form className="max-w-[400px] mx-auto" onSubmit={handleSubmit}>
        <div className="font-semibold text-xl text-center">
          Login to you N Learning account
        </div>
        <div className="border-t-2 my-4 border-slate-300" />
        <TextField
		  autoComplete="off"
          className="my-4"
          placeholder="Username"
          name="username"
          variant="outlined"
          value={data.username}
          inputProps={{
            style: {
              padding: 12,
            },
          }}
          onChange={handleChange}
          fullWidth
          required
        />
        <TextField
		  autoComplete="off"
          className="my-4"
          placeholder="Password"
          name="password"
          type="password"
          variant="outlined"
          value={data.password}
          inputProps={{
            style: {
              padding: 12,
            },
          }}
          onChange={handleChange}
          fullWidth
          required
        />
        <div className="border-t-2 my-4 border-slate-300" />
        <CustomButton
		  type="submit"
          className="
				w-full px-4 py-2 font-semibold text-center text-xl 
				mt-4 bg-violet-600 text-white hover:bg-violet-700 
				disabled:bg-stone-300 disabled:text-black
			"
          disabled={!data.username || !data.password}
        >
          Login
        </CustomButton>
        <div className="mt-8 text-center">
          You do not have an account ?{" "}
          <a
            href="/auth/signup"
            className="font-semibold text-violet-700 underline"
          >
            Register
          </a>
        </div>
      </form>
    </HomeLayout>
  );
};

export default LoginPage;
