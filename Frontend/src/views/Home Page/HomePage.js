import React, { useEffect, useState } from "react";
import HomeLayout from "layouts/Home/HomeLayout";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import * as queries from "graphql/query";

export const homeWallpaper = `${process.env.PUBLIC_URL}/home_wallpaper.png`;

const HomePage = () => {
  const [data, setData] = useState([]);
  const topics = useSelector(({ topic }) => topic.data);

  useEffect(() => {
    const getCourseByTopics = async (topics) => {
      if (!Array.isArray(topics)) return false;

	  const res = await Promise.all(topics.filter((item) => !item.parent)?.map(async (item) => {
		try {
		  const query = {
			topic: [item._id],
			limit: 5,
		  };
		  const result = await queries.getCourses(query);
		  if (result) {
			return {
			  ...item,
			  courses: result
			}
		  }
		} catch (error) {
		  console.log(error);
		}
	  }));
	  setData(res.filter(item => item));
    };

    if (Array.isArray(topics)) {
      getCourseByTopics(topics);
    }
  }, [topics]);

  return (
    <HomeLayout>
      <div className="-m-8 mb-24">
        <div
          className="relative"
          style={{
            width: "100%",
            height: "400px",
            // backgroundColor: "#E3E5ED",
            backgroundImage: `url(${homeWallpaper})`,
            backgroundPosition: "center",
            backgroundSize: "cover",
            backgroundRepeat: "no-repeat",
          }}
        >
          <div className="bg-white p-6 w-[440px] absolute left-[100px] top-[100px]">
            <div className="font-bold text-3xl">Learning that gets you</div>
            <div className="mt-4 text-xl">
              Skills for your present (and your future). Get started with us.
            </div>
          </div>
        </div>
        {topics && (
          <div className="p-8">
            <div className="my-4 py-4 mb-12">
              <div className="font-bold text-3xl">
                Topics recommended for you
              </div>
              <div className="grid grid-cols-5 gap-4 mt-6">
                {topics.filter((item) => !item.parent)?.map((item, index) => (
                    <Link
                      key={index}
                      to={`/courses?topic=${item.slug}`}
                      className="bg-white border-[1px] border-slate-300 text-lg p-4 font-semibold hover:bg-slate-100 flex items-center justify-center"
                    >
                      {item.name}
                    </Link>
                  ))}
              </div>
            </div>

			{Array.isArray(data) && (
			  <>
				{data.map((item, index) => (
				  <div className="my-4 py-4 mb-12" key={index}>
					<div className="font-bold text-3xl">
					  Top courses in <Link to={`/courses?topic=${item.slug}`} className="text-indigo-600 hover:text-indigo-700">{item.name}</Link>
              		</div>
					<div className="grid grid-cols-5 gap-4 mt-2">
					{item.courses?.map((c, cIndex) => (
					  <div className="py-6" key={cIndex}>
						<Link to={`/course/${c.slug}`}>
						  <img 
							src={`${process.env.REACT_APP_API_URL}/file/${c.coverImage}`}
							className="mr-4" 
							alt="course thumbnail" 
							width={260}
							height={145}
						  />
						</Link>
						<div>
						  <Link 
							to={`/course/${c.slug}`} 
							className="font-bold text-lg mb-[4px] text-black my-2 h-[56px] block leading-6"
						  >{c.title}</Link>
						  <div className="text-sm mb-[4px] text-slate-500">{c.createdBy?.name}</div>
						</div>
					  </div>
					))}
					</div>
				  </div>
				))}
			  </>
			)}
          </div>
        )}
      </div>
    </HomeLayout>
  );
};

export default HomePage;
