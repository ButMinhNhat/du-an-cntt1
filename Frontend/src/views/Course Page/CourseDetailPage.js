import React, { useEffect, useState } from "react";
import HomeLayout from "layouts/Home/HomeLayout";
import { Link, useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import * as Actions from "actions";
import StarRating from "components/StarRating";
import moment from "moment";
import KeyboardArrowRightIcon from "@mui/icons-material/KeyboardArrowRight";
import DoneIcon from "@mui/icons-material/Done";
import CustomButton from "components/CustomButton";
import { Collapse } from "@mui/material";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import KeyboardArrowUpIcon from "@mui/icons-material/KeyboardArrowUp";
import PlayCircleIcon from '@mui/icons-material/PlayCircle';
import DescriptionIcon from '@mui/icons-material/Description';
import { transactionEvent } from "ultis/Common";
import CourseRatingStatistics from "components/CourseRatingStatistics";
import CourseRatingList from "components/CourseRatingList";
import UserAvatar from "components/UserAvatar";

export const countAvg = (data) => {
	if(!Array.isArray(data) || data.length === 0) return 0;
	const average = arr => arr.reduce( ( p, c ) => p + c, 0 ) / arr.length;
	
	return average(data).toFixed(1);
}

export const countPercent = (data, value) => {
	if(!Array.isArray(data) || data.length === 0 || !value) return 0;

	const result = data?.filter(item => Math.round(item) === value)?.length / data.length * 100;
	if(isNaN(result)) return 0;
	return Math.round(result);
}

const CourseDetailPage = () => {
	const params = useParams();
	const dispatch = useDispatch();
	const { slug } = params;
	const { COURSE_COMMENT, COURSE_RATING } = transactionEvent;

	const course = useSelector(({ course }) => course.data);
	const transactions = useSelector(({ course }) => course.transactions);
	const isLogin = useSelector(({ auth }) => auth.login);

	const [isTruncated, setIsTruncated] = useState(true);

	useEffect(() => {
		if (slug) {
			dispatch(Actions.getCourseBySlug(slug));
		}
	}, [dispatch, slug]);

	useEffect(() => {
		if (course) {
			dispatch(Actions.getCourseTransactions({
				types: [COURSE_COMMENT, COURSE_RATING],
				courseId: course._id
			}));
		}
	// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [course]);

	if (!course || !transactions) return <HomeLayout></HomeLayout>;

	return (
		<HomeLayout>
			<div className="flex flex-row space-x-4 max-w-[1300px] mx-auto">
				<div className="basis-4/6">
					<div className="mb-4 flex">
						{course.topic?.map((item, index) => (
							<div key={index}>
								{index !== 0 && <KeyboardArrowRightIcon className="mx-2" />}
								<a
									href={`/courses?topic=${item.slug}`}
									className="font-semibold text-violet-700"
								>
									{item.name}
								</a>
							</div>
						))}
					</div>
					<div className="my-4 font-semibold text-3xl">{course.title}</div>
					<div className="my-4 text-lg">{course.shortDescription}</div>
					<div className="my-2 flex items-center">
						<div className='flex items-center'>
							<div className='font-bold mr-2 text-yellow-500 text-lg'>
								{countAvg(transactions?.filter(item => item.type === COURSE_RATING)?.map(item => item.content?.value))}
							</div>
							<StarRating 
								star={countAvg(transactions?.filter(item => item.type === COURSE_RATING)?.map(item => item.content?.value))} 
								size={20}
							/>
							<div className='text-slate-500 ml-1'>({transactions?.filter(item => item.type === COURSE_RATING)?.length})</div>
						</div>
						{" "}
						<span className="ml-4">{course.attendees?.length ?? 0} attendees</span>
					</div>
					<div className="my-2">
						Created by{" "}
						<a
							href={`/user/${course.createdBy?._id}`}
							className="font-semibold text-violet-700 underline"
						>
							{course.createdBy?.name}
						</a>
					</div>
					<div className="my-2">
						Last update:{" "}
						{moment(parseInt(course.updatedAt)).format("DD/MM/YYYY")}
					</div>
					<div className="border-t-[1px] border-slate-300 my-8" />

					<div className="my-8 mb-16">
						<div className="font-semibold my-2 text-2xl">Course Content</div>
						<div className="my-4 border-[1px] border-slate-300 border-t-0">
							{course.sections?.map((item, index) => (
								<CourseContentItem key={index} data={item} />
							))}
						</div>
					</div>

					<div className="my-8 mb-16">
						<div className="font-semibold my-2 text-2xl">Description</div>
						<Collapse in={!isTruncated} collapsedSize="90px">
							<div
								className="my-4"
								dangerouslySetInnerHTML={{ __html: course.description }}
							/>
						</Collapse>
						<div
							className="font-semibold text-violet-700 cursor-pointer"
							onClick={() => setIsTruncated(!isTruncated)}
						>
							{isTruncated ? "Show More" : "Show Less"}
						</div>
					</div>
					
					<div className="my-8 mb-16">
						<div className="font-semibold my-2 text-2xl">Former</div>
						<div className="flex-1 text-lg flex items-center mt-4">
							<UserAvatar url={course?.createdBy?.avatar ?? ""} className="mr-4" />
							<span className="font-semibold text-lg">{course?.createdBy?.name}</span>
						</div>
					</div>

					{(transactions?.filter(item => item.type === COURSE_RATING)?.length > 0) && (
					<React.Fragment>
						<div className="my-8 mb-16">
							<div className="font-semibold my-2 text-2xl">Participant comments</div>
							<CourseRatingStatistics 
								data={transactions?.filter(item => item.type === COURSE_RATING)?.map(item => item.content?.value)} 
							/>
						</div>
						<div className="my-8 mb-16">
							<div className="font-semibold my-2 text-2xl">Notice</div>
							<CourseRatingList data={transactions?.filter(item => item.type === COURSE_RATING)} />
						</div>
					</React.Fragment>
					)}
				</div>
				<div className="basis-2/6">
					<div className="max-w-[400px] mx-auto">
						<img
							className="w-full"
							alt="course cover img"
							src={`${process.env.REACT_APP_API_URL}/file/${course.coverImage}`}
						/>
						<div className="my-4 p-4 border-[1px] border-slate-300 rounded-lg">
							<div className="font-semibold my-2 text-2xl text-violet-700">
								What you will learn
							</div>
							<div>
								{course.features?.map((item, index) => (
									<div className="flex items-center" key={index}>
										<DoneIcon className="my-2 ml-2 mr-4" /> {item}
									</div>
								))}
							</div>
							<Link to={`/learn/${slug}`}>
								<CustomButton 
									className="
										w-full my-4 bg-violet-700 text-xl 
										hover:bg-violet-800 disabled:cursor-not-allowed
										disabled:bg-stone-300 disabled:text-black
									"
									disabled={!isLogin}
								>
									Attend
								</CustomButton>
							</Link>
							{!isLogin && (
							<div className="mt-2 text-center">
								You need to <Link to="/auth/signin" className="text-violet-700 underline font-semibold">Sign In</Link> or{" "}
								<Link to="/auth/signup" className="text-violet-700 underline font-semibold">Sign up</Link> to attend
							</div>
							)}
						</div>
					</div>
				</div>
			</div>
		</HomeLayout>
	);
};

const CourseContentItem = (props) => {
	const { data } = props;
	const [isExpand, setIsExpand] = useState(false);

	return (
		<div>
			<div className="flex items-center px-6 py-4 bg-slate-100 border-t-[1px] border-b-[1px] border-slate-300">
				{isExpand ? (
					<KeyboardArrowUpIcon
						className="mr-2 cursor-pointer"
						onClick={() => setIsExpand(false)}
					/>
				) : (
					<KeyboardArrowDownIcon
						className="mr-2 cursor-pointer"
						onClick={() => setIsExpand(true)}
					/>
				)}
				<span className="flex-1 font-semibold text-lg">{data.title}</span>
				<span>{data.lectures?.length ?? 0} sections</span>
			</div>
			{isExpand && (<div className="px-6 py-2">
				{data.lectures?.map((item, index) => (
					<div key={index} className="flex items-center my-4">
						{item.media ? <PlayCircleIcon /> : <DescriptionIcon />}
						<span className="flex-1 ml-2">{item.title}</span>
					</div>
				))}
			</div>)}
		</div>
	);
};

export default CourseDetailPage;
