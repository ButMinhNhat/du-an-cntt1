import React, { useCallback, useEffect, useState } from "react";
import HomeLayout from "layouts/Home/HomeLayout";
import { Navigate, useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import * as Actions from "actions";
import CourseContentView from "components/CourseContentView";
import _ from "lodash";
import VideoPlayer from "components/VideoPlayer";
import CourseLearnInformation from "components/CourseLearnInformation";
import { trackingEvent, transactionEvent } from "ultis/Common";
import * as mutations from 'graphql/mutation';

export const getCourseLearningList = (course) => {
	if (!course) {
		return [];
	}
	const sections = course.sections;
	const result = [];

	sections.forEach((section) =>
		result.push(...section.lectures, ...(section?.quiz || []))
	);

	return _.sortBy(result, "sequence");
};

export const getCourseLearningListWithoutQuiz = (course) => {
	if (!course) {
		return [];
	}
	const sections = course.sections;
	const result = [];

	sections.forEach((section) =>
		result.push(...section.lectures)
	);

	return _.sortBy(result, "sequence");
};

const CourseLearnPage = () => {
	const params = useParams();
	const dispatch = useDispatch();
	const { slug } = params;
	const { COURSE_COMMENT, COURSE_RATING } = transactionEvent;

	const currentUser = useSelector(({ auth }) => auth.data);
	const course = useSelector(({ course }) => course.data);
	const transactions = useSelector(({ course }) => course.transactions);
	const tracking = useSelector(({ course }) => course.tracking);
	const isLogin = useSelector(({ auth }) => auth.login);

	const [courseLectures, setCourseLectures] = useState(null);
	const [learningIndex, setLearningIndex] = useState(0);

	const handleChangeLecture = (index) => {
		setLearningIndex(index);
	};

	const handleAddAttendee = async (input) => {
		try {
			const { error } = await mutations.saveCourse(input);
			if(error) console.log(error);
		} catch(error) {
			console.log(error);
		}
	};

	const saveCourseLearnTracking = useCallback(async () => {
		try {
			await mutations.saveTracking({
		       ...tracking,
			   course: tracking.course?._id
			});
		} catch(error) {
			console.log(error);
		}
	}, [tracking])

	useEffect(() => {
		if (slug) {
			dispatch(Actions.getCourseBySlug(slug));
		}

		return () => {
			dispatch(Actions.clearCourseData());
		}
	}, [dispatch, slug]);

	useEffect(() => {
		if (course && currentUser) {
			dispatch(Actions.getCourseTransactions({
				types: [COURSE_COMMENT, COURSE_RATING],
				courseId: course._id
			}));
			const lectures = getCourseLearningList(course);
			setCourseLectures(lectures);

			// check course author and attendees list
			if(
				course?.createdBy?._id !== currentUser?._id && 
				course?.attendees?.findIndex(item => item._id === currentUser?._id) < 0
			) {
				const attendeesId = course.attendees.map(item => item._id);
				const input = {
					_id: course._id,
					title: course.title,
					attendees: [...attendeesId, currentUser._id]
				};
				handleAddAttendee(input);
			}

			//get course tracking
			dispatch(Actions.getCourseLearnTracking({
				type: trackingEvent.COURSE_LEARN, 
				courseId: course._id
			}));
		}
	// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [course]);

	useEffect(() => {
	  return () => {
		if(course && tracking) {
		  saveCourseLearnTracking();
		}
	  }
	}, [course, tracking, saveCourseLearnTracking])

	if(!isLogin) return <Navigate to={`/auth/signin`}/>

	if (!course || !courseLectures || !transactions) return <HomeLayout></HomeLayout>;

	return (
		<HomeLayout>
			<div className="-m-8 flex flex-wrap min-h-[600px] mb-2">
				<div className="border-1 w-full flex-1">
					<div className="overflow-y-auto max-h-[650px]">
						{courseLectures[learningIndex].media ? (
							<VideoPlayer fileName={courseLectures[learningIndex].media} />
						) : courseLectures[learningIndex].content ? (
							<div dangerouslySetInnerHTML={{ __html: courseLectures[learningIndex].content }} />
						) : (
							<div></div>
						)}
					</div>
					<CourseLearnInformation />
				</div>
				<div className="w-full md:w-[340px]">
					{Array.isArray(course.sections) && (
						<CourseContentView data={course.sections} onChangeLecture={handleChangeLecture} />
					)}
				</div>
			</div>
		</HomeLayout>
	);
};

export default CourseLearnPage;
