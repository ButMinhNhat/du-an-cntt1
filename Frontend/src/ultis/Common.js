export const transactionEvent = {
	COURSE_COMMENT: "COURSE_COMMENT",
	COURSE_RATING: "COURSE_RATING",
	QUIZ_ANSWER: "QUIZ_ANSWER"
}

export const trackingEvent = {
	COURSE_LEARN: "COURSE_LEARN"
}