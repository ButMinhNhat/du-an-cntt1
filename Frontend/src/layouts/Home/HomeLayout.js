import UserAvatar from "components/UserAvatar";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useLocation, useNavigate } from "react-router-dom";
import * as Actions from "actions";
import qs from 'qs';
import CategoryListView from "components/CategoryListView";
import AlertSnackbar from "components/AlertSnackbar";

const navbarHeight = "76px";
const footerHeight = "86px";

const HomeLayout = (props) => {
  const navigate = useNavigate();
  const location = useLocation();
  const dispatch = useDispatch();

  const topics = useSelector(({ topic }) => topic.data);
  const isLogin = useSelector(({ auth }) => auth.login);

  const handleSearch = (e) => {
	e.preventDefault();
	const input = document.getElementById("default-search")?.value;
	if(input) {
		navigate(`/courses?search=${input}`, { replace: false });
	}
  }

  useEffect(() => {
	const query = qs.parse(location.search, {
		ignoreQueryPrefix: true
	});
	if(query.search){
	  document.getElementById("default-search").value = query.search
	}
  }, [location]);

  useEffect(() => {
    if (!topics) {
      dispatch(Actions.getTopicsList());
    }
  }, [dispatch, topics]);

  return (
    <React.Fragment>
      <div 
	  	className="w-full flex flex-row fixed top-0 py-2 shadow-lg items-center px-4 bg-white z-50"
		  style={{height: navbarHeight}}
	  >
        <Link to="/">
          <img
            src={`${process.env.PUBLIC_URL}/logo.png`}
            alt="logo"
            className="h-[60px]"
          />
        </Link>
        <div className="flex-1 mx-4 flex flex-row items-center">
          <div className="mx-4 cursor-pointer hover:text-indigo-600 group relative py-[18px]">
			<div>Categories</div>
			<div className="hidden group-hover:block hover:block absolute top-16 -left-4">
				<CategoryListView height={`calc(100vh - 4px - ${navbarHeight}`}/>
			</div>
		  </div>
          <div className="flex-1 mx-4" style={{ maxWidth: 600 }}>
            <form autoComplete="off" spellCheck="off" onSubmit={handleSearch}>
              <label
                htmlFor="default-search"
                className="mb-2 text-sm font-medium text-gray-900 sr-only dark:text-gray-300"
              >
                Search
              </label>
              <div className="relative">
                <div className="flex absolute inset-y-0 left-0 items-center pl-3 pointer-events-none">
                  <svg
                    aria-hidden="true"
                    className="w-5 h-5 text-gray-500 dark:text-gray-400"
                    fill="none"
                    stroke="currentColor"
                    viewBox="0 0 24 24"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth="2"
                      d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"
                    ></path>
                  </svg>
                </div>
                <input
                  type="search"
                  id="default-search"
                  className="block p-4 pl-10 w-full text-md text-gray-900 bg-gray-50 rounded-3xl border border-gray-300 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  placeholder="To reseach"
                  required
                />
                <button
                  type="submit"
                  className="text-white absolute right-2.5 bottom-2.5 bg-indigo-700 hover:bg-indigo-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-xl text-sm px-4 py-2 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                >
                  Search
                </button>
              </div>
            </form>
          </div>
		  {isLogin && (
		  <>
			<Link to="/my-learning" className="mx-4 cursor-pointer hover:text-indigo-600">My Learning</Link>
			<Link to="/instructor" className="mx-4 cursor-pointer hover:text-indigo-600">Instructor</Link>
		  </>
		  )}
        </div>
		{isLogin ? (
			<UserAvatar className="h-[40px] w-[40px] mr-2 cursor-pointer" />
		) : (
			<>
				<Link 
					to="/auth/signin" 
					className="mx-2 cursor-pointer border-[1px] border-black px-[20px] py-[10px] font-bold hover:bg-slate-100"
				>Log in</Link>
				<Link 
					to="/auth/signin" 
					className="mx-2 cursor-pointer border-[1px] border-black px-[20px] py-[10px] font-bold bg-black text-white hover:opacity-95"
				>Sign up</Link>
			</>
		)}
      </div>
      <div className="max-w-[1600px] mx-auto mt-[80px] p-8" style={{minHeight: `calc(100vh - 4px - ${navbarHeight} - ${footerHeight})`}}>
        {props.children}
      </div>
      <div className="bg-slate-900 px-4 py-2 flex items-center" style={{height: footerHeight}}>
        <img
          src={`${process.env.PUBLIC_URL}/mini_logo.png`}
          alt="logo"
          className="h-[70px]"
        />
		<span className="font-bold text-white text-2xl ml-4">N Learning</span>
		<span className="flex-1" />
		<span className="text-white text-sm">© 2022 N-Learning, Inc.</span>
      </div>
	  <AlertSnackbar />
    </React.Fragment>
  );
};

export default HomeLayout;
